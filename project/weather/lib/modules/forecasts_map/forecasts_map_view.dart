import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:weather/weather.dart';
import 'package:weather_app/modules/forecasts/forecasts_service/forecasts_service.dart';

class ForecastsMap extends StatefulWidget {
  @override
  State<ForecastsMap> createState() => ForecastsMapState();
}

class ForecastsMapState extends State<ForecastsMap> {
  final ForecastsService _service = ForecastsService();
  final Completer<GoogleMapController> _controller = Completer();

  List<Marker> _markers = [];

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Weather Map'), backgroundColor: Colors.lightBlue[300],),
        body: GoogleMap(
          markers: Set<Marker>.of(_markers),
          mapType: MapType.hybrid,
          initialCameraPosition: _kGooglePlex,
          onMapCreated: _controller.complete,
          onTap: (latLong) {
            _setMarker(Marker(
                markerId: MarkerId("id"),
                position: latLong,
                infoWindow: InfoWindow(title: "Loading")));

            setWeatherDetailsFor(latLong);
          },
        ));
  }

  void _setMarker(Marker marker) {
    setState(() {
      _markers = [marker];
    });
  }

  void setWeatherDetailsFor(LatLng latLng) async {
    var data = await _service.currentWeatherOf(
        latLng.latitude.toString(), latLng.longitude.toString());
        print(data);
    setState(() {
      _markers = [
        Marker(
            markerId: MarkerId("id"),
            position: latLng,
            infoWindow: InfoWindow(
                title: Temperature(data['main']['temp'])
                    .celsius
                    .toInt()
                    .toString()+"˚C"))
      ];
    });
  }
}
